package com.nexsoft;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {


    // make an ArrayList to the Name "data1"
    ArrayList<String> data1 = new ArrayList<>();

    // enter data into "data1"
    data1.add("a");
    data1.add("b");
    data1.add("c");    
    // make an ArrayList to the Name "data2"
    ArrayList<String> data2 = new ArrayList<>();
    
    // enter data into "data1"
    data2.add("c");
    data2.add("d");    
    
    // make an ArrayList to the Name "NewData" copy from data 1
        ArrayList<String> newData = new ArrayList<>(data1);

    // look for different data
        newData.retainAll(data2);
    
    // clearing data1 & data 2
        data1.removeAll(newData);
        data2.removeAll(newData);
    
    // merging data
        data1.addAll(data2);

    // show data
        System.out.println(data1);
    }
}